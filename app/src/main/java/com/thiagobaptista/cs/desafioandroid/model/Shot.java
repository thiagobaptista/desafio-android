package com.thiagobaptista.cs.desafioandroid.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thiagobaptista on 07/08/15.
 */
public class Shot implements Serializable
{
    private int id;
    @SerializedName("views_count")
    private int viewsCount;

    private String title;
    private String description;
    @SerializedName("image_url")
    private String imageUrl;

    private Player player;

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public int getViewsCount()
    {
        return viewsCount;
    }
    public void setViewsCount(int viewsCount)
    {
        this.viewsCount = viewsCount;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public Player getPlayer()
    {
        return player;
    }
    public void setPlayer(Player player)
    {
        this.player = player;
    }

    public static Shot fromJson(String json)
    {
        Gson gson = new Gson();
        return gson.fromJson(json, Shot.class);
    }

    @Override
    public String toString()
    {
        return "Shot{" +
                "id=" + id +
                ", viewsCount=" + viewsCount +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", player=" + player +
                '}';
    }
}
