package com.thiagobaptista.cs.desafioandroid.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thiagobaptista on 07/08/15.
 */
public class ShotsListing
{
    private int page;
    @SerializedName("per_page")
    private int perPage;
    private int pages;
    private int total;

    private List<Shot> shots = new ArrayList<>();

    public int getPage()
    {
        return page;
    }
    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPerPage()
    {
        return perPage;
    }
    public void setPerPage(int perPage)
    {
        this.perPage = perPage;
    }

    public int getPages()
    {
        return pages;
    }
    public void setPages(int pages)
    {
        this.pages = pages;
    }

    public int getTotal()
    {
        return total;
    }
    public void setTotal(int total)
    {
        this.total = total;
    }

    public List<Shot> getShots()
    {
        return shots;
    }
    public void setShots(List<Shot> shots)
    {
        this.shots = shots;
    }

    public static ShotsListing fromJson(String json)
    {
        Gson gson = new Gson();
        return gson.fromJson(json, ShotsListing.class);
    }

    @Override
    public String toString()
    {
        return "ShotsListing{" +
                "page=" + page +
                ", perPage=" + perPage +
                ", pages=" + pages +
                ", total=" + total +
                ", shots=" + shots +
                '}';
    }
}
