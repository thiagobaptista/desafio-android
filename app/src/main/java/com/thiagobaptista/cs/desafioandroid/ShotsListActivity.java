package com.thiagobaptista.cs.desafioandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.thiagobaptista.cs.desafioandroid.model.Shot;
import com.thiagobaptista.cs.desafioandroid.model.ShotsListing;

import org.apache.http.Header;

public class ShotsListActivity extends Activity
{
    private ListView shotsListView;

    private TextView pagesTextView;

    private Button previousButton;
    private Button nextButton;

    private ShotsListing currentShotsListing;

    private ShotsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shots_list);

        adapter = new ShotsListAdapter(this, getLayoutInflater());

        pagesTextView = (TextView) findViewById(R.id.shots_page_textview);

        shotsListView = (ListView) findViewById(R.id.shots_listview);
        if (shotsListView != null)
        {
            shotsListView.setAdapter(adapter);
            shotsListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                        {
                            Shot shot = (Shot) parent.getItemAtPosition(position);
                            Intent intent = new Intent(ShotsListActivity.this, ShotDetailsActivity.class);
                            intent.putExtra("shot", shot);
                            startActivity(intent);
                        }
                    }
            );
        }

        previousButton = (Button) findViewById(R.id.shots_previous_button);
        if (previousButton != null)
        {
            previousButton.setOnClickListener(
                    new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            if (currentShotsListing != null && currentShotsListing.getPage() > 1)
                            {
                                fetchShots(currentShotsListing.getPage() - 1);
                            }
                        }
                    }
            );
        }

        nextButton = (Button) findViewById(R.id.shots_next_button);
        if (nextButton != null)
        {
            nextButton.setOnClickListener(
                    new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            if (currentShotsListing != null
                                    && currentShotsListing.getPage() < currentShotsListing.getPages())
                            {
                                fetchShots(currentShotsListing.getPage() + 1);
                            }
                        }
                    }
            );
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        int page = currentShotsListing == null ? 1 : currentShotsListing.getPage();
        fetchShots(page);
    }

    private void fetchShots(int page)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(
                "http://api.dribbble.com/shots/popular?page=" + page, new TextHttpResponseHandler()
                {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                    {
                        // bummer...
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString)
                    {
                        ShotsListing listing = ShotsListing.fromJson(responseString);
                        currentShotsListing = listing;
                        adapter.setShotsListing(listing);

                        updateView();
                    }
                }
        );
    }

    private void updateView()
    {
        int currentPage = currentShotsListing.getPage();
        int pages = currentShotsListing.getPages();

        if (previousButton != null)
        {
            if (currentPage == 1)
            {
                previousButton.setVisibility(View.GONE);
            }
            else
            {
                previousButton.setVisibility(View.VISIBLE);
            }
        }

        if (nextButton != null)
        {
            if (currentPage == pages)
            {
                nextButton.setVisibility(View.GONE);
            }
            else
            {
                nextButton.setVisibility(View.VISIBLE);
            }
        }

        if (pagesTextView != null)
        {
            String text = currentPage + " of " + pages;
            pagesTextView.setText(text);
        }
    }
}
