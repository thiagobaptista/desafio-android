package com.thiagobaptista.cs.desafioandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thiagobaptista.cs.desafioandroid.model.Player;
import com.thiagobaptista.cs.desafioandroid.model.Shot;

/**
 * Created by thiagobaptista on 08/08/15.
 */
public class ShotDetailsActivity extends Activity
{
    private Shot shot;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);

        shot = (Shot) getIntent().getSerializableExtra("shot");
        if (shot != null)
        {
            ImageView imageView = (ImageView) findViewById(R.id.shot_list_image);
            if (imageView != null)
            {
                String url = shot.getImageUrl();
                Picasso.with(this)
                       .load(url)
                       .placeholder(R.drawable.dribbble_logo)
                       .into(imageView);
            }

            TextView titleTextView = (TextView) findViewById(R.id.shot_list_title);
            if (titleTextView != null)
            {
                titleTextView.setText(shot.getTitle());
            }

            TextView viewsCountTextView = (TextView) findViewById(R.id.shot_list_views_count);
            if (viewsCountTextView != null)
            {
                viewsCountTextView.setText("" + shot.getViewsCount());
            }

            TextView descriptionTextView = (TextView) findViewById(R.id.shot_details_description);
            if (descriptionTextView != null)
            {
                Log.d("_CS", "descriptionTextView");
                descriptionTextView.setText( shot.getDescription() ) ;
            }

            Player player = shot.getPlayer();
            if (player != null)
            {
                Log.d("_CS", "Player's not null");
                ImageView playerThumbImageView = (ImageView) findViewById(R.id.shot_details_player_thumb);
                if (playerThumbImageView != null)
                {
                    String url = player.getAvatarUrl();
                    Picasso.with(this)
                           .load(url)
                           .placeholder(R.drawable.dribbble_logo)
                           .into(playerThumbImageView);
                }

                TextView playerNameTextView = (TextView) findViewById(R.id.shot_details_player_name);
                if (playerNameTextView != null)
                {
                    playerNameTextView.setText( player.getName() ) ;
                }
            }
        }
    }
}
