package com.thiagobaptista.cs.desafioandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thiagobaptista.cs.desafioandroid.model.Shot;
import com.thiagobaptista.cs.desafioandroid.model.ShotsListing;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thiagobaptista on 07/08/15.
 */
public class ShotsListAdapter extends BaseAdapter
{
    private Context context;

    private LayoutInflater layoutInflater;

    private List<Shot> shots = new ArrayList<>();

    public ShotsListAdapter(Context context, LayoutInflater layoutInflater)
    {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount()
    {
        return shots.size();
    }

    @Override
    public Object getItem(int position)
    {
        return shots.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Shot shot = (Shot) getItem(position);

        View view = convertView;
        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.layout_shots_list, null);
        }

        ImageView imageTextView = (ImageView) view.findViewById(R.id.shot_list_image);
        if (imageTextView != null)
        {
            String url = shot.getImageUrl();
            Picasso.with(context)
                   .load(url)
                   .placeholder(R.drawable.dribbble_logo)
                   .into(imageTextView);
        }

        TextView titleTextView = (TextView) view.findViewById(R.id.shot_list_title);
        if (titleTextView != null)
        {
            titleTextView.setText( shot.getTitle() );
        }

        TextView viewsCountTextView = (TextView) view.findViewById(R.id.shot_list_views_count);
        if (viewsCountTextView != null)
        {
            viewsCountTextView.setText("" + shot.getViewsCount());
        }

        return view;
    }

    public void setShotsListing(ShotsListing listing)
    {
        shots = listing.getShots();
        notifyDataSetChanged();
    }
}
